USER := $(shell id -u):$(shell id -g)
MARKDOWNLINT_IMAGE := davidanson/markdownlint-cli2:0.3.2
MARKDOWNLINT_RUN := docker run --rm --volume "$${PWD:?}:$${PWD:?}:ro" --workdir "$${PWD:?}" $(MARKDOWNLINT_IMAGE)
MARKDOWNLINT_FIX := docker run --entrypoint=markdownlint-cli2-fix --rm --user "$(USER)" --volume "$${PWD:?}:$${PWD:?}:rw" --workdir "$${PWD:?}" $(MARKDOWNLINT_IMAGE)

markdownlint:
	$(MARKDOWNLINT_RUN) "**/*.md"

markdownlint-fix:
	$(MARKDOWNLINT_FIX) "**/*.md"
