# Integration process

## Updates

Original Exocomp was purpose build to update image and charts versions.

Renovate got updates and learned how to update image in kubernetes manifests (not enabled yet) and chart versions in helmfiles.

Renovate updates versions in `download-scripts.sh` (Regex manager) and executes them via post update hooks.

### New Exocomp

Exocomp got a new role: running scripts create merge requests and opening issues at gitlab.

- generates `.gitlab-ci.yml`
- configures renovate bot (actives post update hooks, enable go mod)
- ensures README and LICENSE
- formates code
- ...

## Merge avalanche

Many dependencies mean many updates in general.

A change that affects many services happens in parallel.

Renovate originally merged only on branch, and rebased the others.

Renovate updates the services (git submodules) for customize.  
Customize pipeline takes 2.5 hours.  
= 9 merges per day (in case all pipelines always succeed)

Running once an hour was fine for the services but customize was overwhelmed.

## Integration repos

`customize` became https://gitlab.com/utopia-planitia/test-infra/integration-cluster

`customize/.gitlab-ci.yml` became https://gitlab.com/utopia-planitia/test-infra/integration-pipeline

- integration-pipeline uses integration-cluster via git clone
- checkout branch `verified`
- deploys Ubuntu, Kubernetes, Services
- runs tests
- marks `main` as `release-canidate`
- checkout branch `release-canidate`
- updates Ubuntu, Kubernets, Services
- runs tests
- marks `release-canidate` as `verified`
- restarts pipeline

integration-pipeline https://gitlab.com/utopia-planitia/test-infra/integration-pipeline/-/blob/main/.gitlab-ci.yml

integration-cluster branches https://gitlab.com/utopia-planitia/test-infra/integration-cluster/-/branches

This skips commits but marks known good states and can keep up with the changes.
