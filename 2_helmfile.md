# Helmfile

Found at github https://github.com/roboll/helmfile

- Manages access to charts and configures chart releases.
- Calls helm internally.
- Helm stores secrets in namespace to keep track of manifests.
- Helm removes deployments for manifests that have been used before are missing now.

## Example

```
nano helmfile.yaml
helmfile sync
```

## Can access chart repositories

```
repositories:
- name: stable
  url: https://charts.helm.sh/stable
```

## Deploys Charts

```
releases:
  - name: my-vault
    namespace: vault
    labels:
      foo: bar
    chart: roboll/vault-secret-manager
    version: 1.24.1
    ...
```

## Allows to configure Charts

```
releases:
  - name: my-vault
    ...
    values:
      - vault.yaml
      - address: https://vault.example.com
      - image:
          tag: {{ requiredEnv "IMAGE_TAG" | quote }}
```

## Allows to patch charts

Uses helm-x, chartify, and kustomize to patch charts.

https://github.com/roboll/helmfile/pull/673

```
nano fix-pod-anti-affinity.yaml
```

```
releases:
  - name: kube-prometheus-stack
    ...
    strategicMergePatches:
      - fix-pod-anti-affinity.yaml
```
