# History

## P1: How to deploy stuff at all?

```
hetznerctl service all|<name> -> kubectl apply -f manifests

curl -o manifests/some-service.yaml http://some-service/manifests.yaml

nano manifests/individual-stuff.yaml
```

## P1: How to patch downloaded manifests & be able to still download updates?

```
curl -o src/some-service.yaml http://some-service/manifests.yaml

nano src/kustomized.yaml src/patch-some-service.yaml

kustomize src/ > manifests/kustomize.yaml
```

## P2: Helm v2 used tiller. Who to use Charts?

```
nano helmfile.yaml

helmfile template > [src|manifests]/helmfile.yaml
```

## P4: Some manifests could not be static (certificates).

use service folder as template and create "generated" directory
use "generated" directory as service folder
needed to be checked in to git

## P5: How to delete a thing?

```
unlink manifests/thing.yaml

echo "${KIND} ${NAME}" > deletions.txt

cat deletions.txt | xargs -L 1 kubectl delete {}
```

## P6: Charts moved from helm 2 to helm 3 and used the chance to change a lot.

## How to maintain deletions.txt?

Don't
