# New structure

Example https://gitlab.com/utopia-planitia/observability/monitoring/-/tree/main

One directory per helmfile, one purpose per helmfile

`hetznerctl svc [all|<name>]` stayed the same

- removed all the previous complexity https://gitlab.com/utopia-planitia/hetznerctl/-/commit/233db754559c802be1abc83bcec77268d245dc57
- searches per service for `*/helmfile.yaml[.tpl]` and runs `helmfile apply`

`cluster.yaml` is passed to `helmfile sync` and can be used in `helmfile.yaml`  
https://gitlab.com/utopia-planitia/observability/alerting/-/blob/main/alerting/helmfile.yaml.tpl

## Local manifests

Directory can be used as a chart  
https://gitlab.com/utopia-planitia/observability/monitoring/-/tree/main/ingress

## Phases

Addons from within hetznerctl are a service now.  
https://gitlab.com/utopia-planitia/addons

CRDs are needed by other service (resursion between monitoring and rook-ceph)

Default storage class only applys default to volume claims when applied before volume was requested.

- crds
- init
- not crds && not init

## mini service pipeline

Runs

- helmfile lint
- helmfile template

Uses `ci/cluster.yaml` to mock `cluster.yaml` of hetznerctl.  
https://gitlab.com/utopia-planitia/observability/alerting/-/blob/main/.gitlab-ci.yml
